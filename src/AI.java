import java.util.ArrayList;
import java.util.List;

public abstract class AI {
    protected int[] code;

    protected int[] generateCode() {
        List<Integer> choices = new ArrayList<>();
        int[] code = new int[4];
        //I want a list of available digits to choose, that I can remove digits from if they are chosen.
        for (int h = 0; h < 10; h++) {
            choices.add(h);
        }

        for (int i = 0; i < code.length; i++) {
            //Randomly pick a number from the choices menu
            int temp = (int) (Math.random() * choices.size());
            code[i] = choices.get(temp);
            choices.remove(temp);
        }
        return code;
    }
    public int[] getGuess(){
        int[] guess = {-1,-1,-1,-1};
        return guess;
    }
}


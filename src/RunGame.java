import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RunGame  {
    private Player p;
    private AI c ;
    private int[] computer_code;
    private int[] player_code;
    private final int NUMBER_OF_GUESSES = 7;//Number of guesses can be changed
    private final int LENGTH_OF_CODE =4;//DO NOT CHANGE LENGTH_OF_CODE

    //store game in outputString that player can choose to write to file
    private String outputString="";

    public RunGame(DifficultySetting difficulty, InputType playerType) {
        if(difficulty== DifficultySetting.EASY){c = new Novice();
            output("You are playing on Novice difficulty");}
        else if(difficulty==DifficultySetting.MEDIUM){c = new Intermediate();
            output("You are playing on Intermediate difficulty");}
        else if(difficulty==DifficultySetting.HARD){c=new Expert();
            output("You are playing on Expert difficulty");}
         if(playerType==InputType.KEYBOARD){p = new Player();}
         else if(playerType==InputType.FILE){p= new PlayerFromFile();}
    }


    public void proceed() {
//this is the actual game loop


        computer_code = c.generateCode();
        player_code = p.setCode();
        

        int i=0;
        while(i<NUMBER_OF_GUESSES) {
            output("Round "+(i+1));
            output("-------------");
            int[] player_guess = p.getGuess();
            output("You guessed: "+java.util.Arrays.toString(player_guess));

            if(testForWin(feedback(player_guess, computer_code))==true){
                output("You have won");
                break;
            }
            //I wonder if I can remove this duplication too?
            int[] computer_guess =  c.getGuess();
            output("The computer guessed: "+java.util.Arrays.toString(computer_guess));

            if(testForWin(feedback(computer_guess, player_code))==true){
                output("The computer has won");
                break;
            }
            if(c instanceof Expert){((Expert) c).eliminateOptions(compareGuess(computer_guess, player_code),computer_guess );}
            output(" ");
            i++;
        }
        if(i>=NUMBER_OF_GUESSES) {
            output("It seems you have both used up all your guesses.");
            output("So I guess we'll call it a draw");

        }
        output("The computer's secret code was " + java.util.Arrays.toString(computer_code));
        publishGame();
        System.out.println("Goodbye, thanks for playing.");
    }


    public boolean testForWin(int[] result){
        if(result[0]==LENGTH_OF_CODE){
            output("Success!");
        return true;}
        return false;

    }
    public int[] feedback(int[]guess, int[] code){
        int[] fb = compareGuess(guess,code);
        output(fb[0] + " Bulls and " + fb[1] + " Cows!");
        return fb;
    }
    public static int[] compareGuess(int[] guess, int[] code) {
        int Bulls = 0;
        int Cows = 0;

        for (int i = 0; i < guess.length; i++) {
            for (int j = 0; j < code.length; j++) {

                if (guess[i] == code[j] && i == j) {
                    Bulls = Bulls + 1;
                    break;
                } else if (guess[i] == code[j]) {
                    Cows = Cows + 1;
                    break;
                }

            }
        }

        //This has to be down the bottom otherwise it won't let me assign Bulls and Cows if it's already been initialised
        int[] result = {Bulls, Cows};

        return result;
    }

    //Below function was going to be setter for code length, but never used
    //public int getLENGTH_OF_CODE(){return LENGTH_OF_CODE;}

    public void publishGame(){
        System.out.println("Would you like to save the record of your game? (y/n)");
        String query = Keyboard.readInput();
        if (query.toLowerCase().startsWith("y")){
            System.out.println("Please enter a filename to save to:");
            String fileName = Keyboard.readInput();
            FileOut(fileName);
            System.out.println("Game history saved.");
        }
        else{
            System.out.println("Okay, we won't save it");
        }
    }

    public void FileOut(String filename){
        File mine = new File(filename+".txt");
        //need to break up outputString to write on multiple lines
        String[] breakUpString = outputString.split("\n");
        try(PrintWriter writer = new PrintWriter(new FileWriter(mine))) {
            for(int i=0; i<breakUpString.length;i++){

                writer.println(breakUpString[i]);}
        } catch (IOException e) {
            System.out.println("Output error");
        }
    }




    //This function is so some sentences that are printed to screen can be stored for saving to file later
    public void output(String s){
        System.out.println(s);
        outputString= outputString +s +"\n";
    }
}

import java.util.ArrayList;
import java.util.Arrays;

public class Expert extends AI {
    private ArrayList<int[]> possibilities = generateAllPossibilities();

    @Override
    public int[] getGuess(){
        int choice = (int)(Math.random()*possibilities.size());
        int[] guess = possibilities.get(choice);
        return guess;
    }

    public ArrayList<int[]> generateAllPossibilities(){
     ArrayList<int[]> possibilities = new ArrayList<>();
        //Generate every possible combination of
     for(int i=0; i<10;i++){
            for(int j=0; j<10; j++){
    if(i!=j){
                for(int k=0; k<10;k++){
                    if(k!=i && k!=j){
                    for(int l=0;l<10;l++){
                        if(l!=i && l!=j && l!=k){
                        int[] temp={i,j,k,l};
                        possibilities.add(temp);

                    }}}}
                }
            }
        }

        return possibilities;
    }
    //trim the list of possibilities
    public void eliminateOptions(int[] feedback, int[] originalGuess){
        //create a temporary array the same as our list of possibilities
        //so can remove options from this array as we iterate through it
        ArrayList <int[]> temp = (ArrayList<int[]>) (possibilities.clone());

        for(int[] option :possibilities) {
            int[] pseudoFeedback = RunGame.compareGuess(originalGuess, option);
            if (!(Arrays.equals(pseudoFeedback, feedback))) {

                temp.remove(option);
            }


        }

        possibilities=temp;

    }
}

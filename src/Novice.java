public class Novice extends AI {
    @Override
    public int[] getGuess(){
        //reusing the generateCode method though might be better to put that into some kind of shared method.
        int[] guess = generateCode();
        return guess;
    }
}

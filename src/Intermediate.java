import java.util.ArrayList;

public class Intermediate extends AI {
    //memory stores guess. Since memory is initialised when Intermediate is initialised at the moment it is not wiped between games
    private ArrayList<int[]> memory = new ArrayList<>();

    @Override
    public int[] getGuess() {
        int[] guess = {0,0,0,0};
        boolean guessIsNew = false;
        while (!guessIsNew) {
            guess = generateCode();

            //just checking to make sure that duplicates are filtered by the following code
            if(memory.contains(guess)){
                System.out.println("This is just a test and should be deleted");
            }
            if (!memory.contains(guess)) {
                guessIsNew = true;
                memory.add(guess);
                break;
            }
        }

        return guess;
    }
}

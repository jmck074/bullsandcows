import java.io.File;
import java.io.IOException;
import java.util.*;

public class PlayerFromFile extends Player {
    //Chose to use deque so guesses can be easily removed as they are used.
    private Deque<String> listOfGuesses = fileIn();

    public PlayerFromFile(){

    }

    @Override
    public int[] getGuess() {
        if(!listOfGuesses.isEmpty()){
        String rawGuess = listOfGuesses.pollFirst();
        int[] actualGuess = convertStringToIntArray(rawGuess);

        return actualGuess;}
        else{

            //System.out.println("Your file was too short, you'll have to enter this guess manually");
            //if deque of guesses is empty, reverts to getGuess method of super class.
            return super.getGuess();}
    }

    public Deque<String> fileIn(){
        Deque<String> inputList = new ArrayDeque<>();
        System.out.println("Enter the file name (not including .txt suffix)");
        String inputFileName = Keyboard.readInput();
        File fileInput = new File(inputFileName+".txt");
        try(Scanner scanner = new Scanner(fileInput)){
            while(scanner.hasNextLine()){
                inputList.add(scanner.nextLine());
            }
        }catch(IOException e){
            System.out.println("There was a problem reading from the file.");
            System.out.println("That's okay, you can still input guesses manually.");
            e.getMessage();
        }
        return inputList;
    }
}

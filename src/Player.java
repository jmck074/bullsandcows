
public class Player {

    public Player(){

    }


    public  int[] getGuess(){
        //int[] guess = new int[4];
        int[] guess ={-1,-1,-1,-1};
        System.out.printf("Enter your guess ( 4 digits):");
        guess = getValidInput();



        return guess;
    }

    public int[] getValidInput(){
        //int[] intArrayVariable;
        String userInput = Keyboard.readInput();

        //Check user input is the right length
        if(userInput.length()!=4){
            System.out.println("Please enter the correct number of digits (default is 4)");
            return getValidInput();
        }
        //check user input doesn;t contain duplicate entries
        for(int i=0;i<userInput.length();i++){
            if((userInput.substring(0,i)+userInput.substring(i+1)).contains(""+(userInput.charAt(i)))){
                System.out.println("Please do not use the same character more than once");
                return getValidInput();

            }
        }
        //check userinput can be converted to int array NB use of int array will be a problem if we allow characters
        try{
            return convertStringToIntArray(userInput);
        }catch (NumberFormatException e){
            System.out.println("Please use only digits");
            return getValidInput();
        }
    }

    public int[] setCode(){

        System.out.println("Choose a secret code");

        int[] playerCode = getValidInput();
        return playerCode;
    }

    public int[] convertStringToIntArray(String s){
        int[] someArray = new int[4];
        for(int i=0; i<4;i++){
            someArray[i]=Integer.parseInt(s.substring(i,i+1));
        }
        return someArray;
    }


}


public class GameSetup {


    private void start() {
        //start menu
        System.out.println("Welcome to the adventure!");
        DifficultySetting difficulty = getDifficulty();
        InputType inputDevice = chooseUserInput();
        RunGame game = new RunGame(difficulty, inputDevice);


        game.proceed();
    }

    public DifficultySetting getDifficulty(){

            System.out.println("Choose a difficulty level.");
            System.out.println("1: Easy");
            System.out.println("2: Medium");
            System.out.println("3: Hard");
            System.out.println(">");
            String difficultyChoice = Keyboard.readInput();
            if(difficultyChoice.equals("1")){return DifficultySetting.EASY;}
            else if(difficultyChoice.equals("2")){return DifficultySetting.MEDIUM;}
            else if(difficultyChoice.equals("3")){return DifficultySetting.HARD;}
            else{
                System.out.println("Sorry I didn't catch that. Let's try again.");
                System.out.println();
                return getDifficulty();
            }
        }


    public InputType chooseUserInput(){
        System.out.println("Choose if you would like to input your guesses manually or load from a file");
        System.out.println("1: I got this. I'll enter it myself.");
        System.out.println("2: I have a file ready to go");
        String inputOption = Keyboard.readInput();
        if(inputOption.equals("1")){return InputType.KEYBOARD;}
        else if(inputOption.equals("2")){return InputType.FILE;}
        else{
            System.out.println("Sorry, I didn't catch that. Please try again\n");

            return chooseUserInput();
        }
    }


    public static void main(String[] args) {
        new GameSetup().start();
    }
}